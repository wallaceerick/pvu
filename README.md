# Plant vs Undead
> The complete HUB for PVU

```js
yarn install // Project setup
```

```js
yarn serve // Compiles and hot-reloads for development
```

```js
yarn build // Compiles and minifies for production
```

```js
yarn lint // Lints and fixes files
```


