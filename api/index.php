<?php
    header('Access-Control-Allow-Origin: *'); 
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization'); 

    include_once('simple_html_dom.php');

    $html = file_get_html('https://bscscan.com/readContract?&a=0x5ab19e7091dd208f352f8e727b6dcc6f8abb6275');
    $e    = $html->find('div#readCollapse15', 0);
    $ids  = $e->find('div.form-group', 0)->find('a', 0);
    
    if($ids) {
        $result = $ids->title;
        $array = explode(',', $result);
        echo json_encode($result, true);
    } else {
        echo json_encode(false, true); 
    } 

?>