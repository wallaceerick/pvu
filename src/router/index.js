import Vue from 'vue'
import VueRouter from 'vue-router'
import Pages from './pages'
import Projects from './projects'

Vue.use(VueRouter)

const router = new VueRouter({
	routes: [...Pages, ...Projects],
	mode: 'history',
	linkExactActiveClass: 'is-active'
})

export default router
