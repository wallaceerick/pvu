export default [
	{
        path: '/home',
        alias: '/',
        name: 'Home',
        component: () => import('@/pages/Home')
    }
]
