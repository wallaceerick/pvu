export default [
	{
		path: '/analyser',
		name: '<span>Seed</span> Analyser',
		status: true,
		description: 'Check the quantity of plants available in the pool, so you can decide whether or not it\'s worth claiming your seed.',
		component: () => import('@/pages/Analyser')
	},
	{
		path: '/lookup',
		name: '<span>Plant</span> Lookup',
		status: true,
		description: 'A simple tool to identify your seed information based on its ID.',
		component: () => import('@/pages/Lookup')
	},
	{
		path: '/simulator',
		name: '<span>Garden</span> Simulator',
		status: false,
		description: 'Under construction...',
		component: () => import('@/pages/Simulator')
	},
	{
		path: '/prediction ',
		name: '<span>Time</span> Prediction',
		status: false,
		description: 'Under construction...',
		component: () => import('@/pages/Prediction')
	},
	{
		path: '/events ',
		name: '<span>Weather</span> Events',
		status: false,
		description: 'Under construction...',
		component: () => import('@/pages/Prediction')
	}
]
