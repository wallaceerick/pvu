import Vue from 'vue'

Vue.mixin({
  methods: {
    BRL(value) {
      return value.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
    },
    USD(value) {
      return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' })
    },
    combineValues(array){
      return array.reduce((name, value) => (name[value] = name[value] + 1 || 1, name), {})
    },
    splitPlantID(plant) {
      let 
        array  = plant.toString().replace(/^\s+|\s+$/g, '').split(''),
        id     = `${array[0]}${array[1]}${array[2]}`,
        type   = `${array[3]}${array[4]}`,
        img    = `${array[5]}`,
        rarity = `${array[6]}${array[7]}`
      return { id, type, img, rarity }
    },
    getID(id){
      let r = Number(id)
      return r >= 200 ? 'Mother Tree' : 'Plant'
    },
    getImage(type, image){
      return `https://wallaceerick.com.br/clientes/pvu/plants/${type}_${image}.png`
    }, 
    getBadge(type){
      return `https://wallaceerick.com.br/clientes/pvu/badges/${type.toLowerCase()}.svg`
    },
    getFrame(rarity){
      return `https://wallaceerick.com.br/clientes/pvu/frames/${rarity.toLowerCase()}.png`
    },
    getRarity(rarity) {
      let r = Number(rarity)
      console.log(r, r >= 60)
      return r >= 60 && r <= 88 ? 'Uncommon' : r >= 89 && r <= 98 ? 'Rare' : r == 99 ? 'Mythic' : 'Common'
    },
    getType(type) {
      let 
        t = Number(type),
        ids = [
          ['Fire',      0, 1, 7, 17, 30, 90],
          ['Ice',       2, 6, 29, 92],
          ['Water',     4, 5, 36, 38, 39],
          ['Electro',   3, 8, 15, 32, 34],
          ['Wind',      9, 10, 16, 37],
          ['Parasite',  11, 12, 13, 22, 23, 24],
          ['Light',     14, 18, 19, 20, 21, 91],
          ['Dark',      31, 33, 35, 93],
          ['Metal',     25, 26, 27, 28]
        ]
      
      for (let i = 0; i < ids.length; i++) {
        let 
          arr = ids[i],
          found = arr.includes(t)

        if(found) {
          return arr[0]
        }
      }
    }
  },
  getLE(type){
    let 
      t = Number(type),
      ids = [
        ['Fire',      [400, 440, 511, 701], [650, 700, 811, 1001], [750, 1040, 1211, 1401]],
        ['Ice',       [500, 550, 591, 751], [800, 850, 911, 1151], [1050, 1340, 1511, 1701]],
        ['Water',     4, 5, 36, 38, 39],
        ['Electro',   3, 8, 15, 32, 34],
        ['Wind',      9, 10, 16, 37],
        ['Parasite',  11, 12, 13, 22, 23, 24],
        ['Light',     14, 18, 19, 20, 21, 91],
        ['Dark',      31, 33, 35, 93],
        ['Metal',     25, 26, 27, 28]
      ]
    console.log(t, ids)
  }
})