import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App'
import router from './router'
import './mixins'

Vue.use(VueAxios, axios)

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

Vue.config.productionTip = false
Vue.config.devtools = true